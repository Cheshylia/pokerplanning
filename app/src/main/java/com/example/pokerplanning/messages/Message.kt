package com.example.pokerplanning.messages

import com.google.gson.*
import java.lang.reflect.Type

data class Message constructor(var msg:String, var address:String) {
    companion object {
        private val gson = Gson()
        fun toJson(src: Message) = gson.toJson(src)
        fun fromJson(str: String) = gson.fromJson<Message>(str, Message::class.java)
    }
}

/*
sealed class Message constructor(val type: String, var address:String) {
    companion object {
        private val gson = GsonBuilder().registerTypeAdapter(Message::class.java, MessageSerializer()).create()
        fun toJson(src: Message) = gson.toJson(src)
        fun fromJson(str: String) = gson.fromJson<Message>(str, Message::class.java)
    }
}

class UnknownMessage(): Message("unknown","")

//class srvBcast(val msg: String): Message("srvBcast","")

//class endVote(val msg: String, val listeAdresses: List<voteList>): Message("stop","")

class sendScores(val msg: String, val listeAdresses: List<voteList>, val values: List<Int>): Message("votes","")

//class connection(val msg: String, val clientID: String): Message("id","")

//class vote(val msg: String, val clientID: String, val voteVal: Int): Message("vote","");

data class voteList(val name: String, val note: Int)

class MessageSerializer: JsonDeserializer<Message> {

    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext    ): Message {
        val type = json.asJsonObject["type"].asString
        return when (type) {
            //server side
//            "srvBcast" -> context.deserialize<srvBcast>(json, srvBcast::class.java)
//            "stop" -> context.deserialize<endVote>(json, endVote::class.java)
            "sendScores" -> context.deserialize<sendScores>(json, sendScores::class.java)

            //client side
//            "id" -> context.deserialize<connection>(json, connection::class.java)
//            "votes" -> context.deserialize<vote>(json, vote::class.java)
            else -> UnknownMessage()
        }
    }

}
*/