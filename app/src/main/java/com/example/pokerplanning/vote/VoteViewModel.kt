package com.example.pokerplanning.vote

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.pokerplanning.salon.ClientSocket

class VoteViewModel(context: Context) : ViewModel() {
    private val _valVote = MutableLiveData<Int>(0)
    val scoreToSend : Int get() = _valVote as Int
    private val _ServerMsg = MutableLiveData<String> ("")
    val mmsg : LiveData<String> get() = _ServerMsg as LiveData<String>
    private val _msgSet = mutableSetOf<String>()
    private val _allMessages = MutableLiveData<List<String>>()

    private val _connected = MutableLiveData<Boolean>(false)

    private val _clientSocket = context?.let {
        ClientSocket(
            it, this::onReceiveMessage, this::onConnectionStatus)
    }

    fun connect(serverIp:String, serverPort:Int?, value : Int){
        _clientSocket?.connect(serverIp, serverPort, value)
    }

    fun getCliSoc(): ClientSocket {
        return _clientSocket
    }

    override fun onCleared() {
        _clientSocket?.stop("cleared")
        _connected.postValue(false)
        super.onCleared()
    }

    fun onReceiveMessage(msg: String) {
        _msgSet.add(msg)
        _allMessages.postValue(_msgSet.toList())
    }

    fun onConnectionStatus(bool:Boolean) {
        _connected.postValue(bool)
    }

    fun getValueToSend(): Int {
//        return scoreToSend
        return _valVote.value!!
    }


    fun changeValue(myVal: Int){
        _valVote.setValue(myVal)
    }

    fun readMessage(msg: String){
        _ServerMsg.postValue(msg)
    }

    fun returnSrvMsg(): LiveData<String>{
        return mmsg
    }

    fun send(msg:String){
        _clientSocket?.send(msg)
    }


}
