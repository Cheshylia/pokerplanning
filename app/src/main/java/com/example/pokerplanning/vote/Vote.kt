package com.example.pokerplanning.vote

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.example.pokerplanning.R
import com.example.pokerplanning.databinding.VoteFragmentBinding
import com.example.pokerplanning.salon.ClientSocket
import com.example.pokerplanning.serveur.ServerSocket

class Vote : Fragment() {

    private lateinit var viewModel: VoteViewModel
    private lateinit var binding: VoteFragmentBinding
    private lateinit var clientSocket: ClientSocket

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)
        // Inflate view and obtain an instance of the binding class
        binding = DataBindingUtil.inflate(inflater, R.layout.vote_fragment,container, false)
        Log.i("Titre Fragment", "Called ViewModelProviders.of")

        viewModel = ViewModelProviders.of(this, VoteViewModelFactory(this.context)).get(VoteViewModel::class.java)


        val argRec = view?.findViewById<View>(R.id.action_salon_to_vote)
////        clientSocket = arguments?.getBundle("socket")
//        val myBundle = arguments?.getBundle("socket")
//        clientSocket = myBundle?.get("socket") as ClientSocket
//        clientSocket = viewModel.getCliSoc().getSocket()
        val mip = arguments?.getString("ip")


        // Set the viewmodel for databinding - this allows the bound layout access
        // to all the data in the VieWModel
        binding.voteViewModel = viewModel

        binding.cardA.setOnClickListener{
            viewModel.changeValue(1)
            Log.i("Card value", "Selected value : 1")
        }
        binding.card2.setOnClickListener{
            viewModel.changeValue(2)
            Log.i("Card value", "Selected value : 2")
        }
        binding.card3.setOnClickListener{
            viewModel.changeValue(3)
            Log.i("Card value", "Selected value : 3")
        }
        binding.card5.setOnClickListener{
            viewModel.changeValue(5)
            Log.i("Card value", "Selected value : 5")
        }
        binding.card8.setOnClickListener{
            viewModel.changeValue(8)
            Log.i("Card value", "Selected value : 8")
        }
        binding.cardK.setOnClickListener{
        }
        binding.submitVote.setOnClickListener{
            view -> viewModel.connect(mip!!, ServerSocket.PORT, viewModel.getValueToSend())
            view?.findNavController()?.navigate(R.id.action_vote_to_resultat)
        }
        clientSocket = viewModel.getCliSoc()
        binding.lifecycleOwner = this
        (activity as? AppCompatActivity)?.supportActionBar?.title = "Vote"
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(VoteViewModel::class.java)

    }
    /** Methods for updating the UI **/
//    fun onNext() {
//        view -> viewModel.connect(mip, ServerSocket.PORT, )
//        view?.findNavController()?.navigate(R.id.action_vote_to_resultat)
//        //clientSocket.sendAndReceive(viewModel.getValueToSend().toString())
//
//        //clientSocket.sendAndReceive(clientSocket.socRet, viewModel.getValueToSend().toString())
//    }
}
