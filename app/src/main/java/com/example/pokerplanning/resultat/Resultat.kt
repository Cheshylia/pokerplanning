package com.example.pokerplanning.resultat

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil

import com.example.pokerplanning.R
import com.example.pokerplanning.databinding.ResultatFragmentBinding
import com.example.pokerplanning.salon.ClientSocket

class Resultat : Fragment() {

    private lateinit var viewModel: ResultatViewModel
    private lateinit var binding: ResultatFragmentBinding
    private lateinit var clientSocket: ClientSocket

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.inflate(inflater, R.layout.resultat_fragment, container, false)
        Log.i("Resultat Fragment", "Called ViewModelProviders.of")

        viewModel = ViewModelProviders.of(this, ResultatViewModelFactory(this.context)).get(ResultatViewModel::class.java)

        binding.resultatViewModel = viewModel

        val items = mapOf("A" to 1, 2 to 2, 3 to 3, 5 to 5, 8 to 8, "K" to 13)
        val resList = viewModel.getAllMessages()

//        binding.progressBarA.progress = items.getValue("A")
        binding.progressBarA.progress = resList!![0].toInt()
        binding.progressBar2.progress = resList[1].toInt()
        binding.progressBar3.progress = resList[2].toInt()
        binding.progressBar5.progress = resList[3].toInt()
        binding.progressBar8.progress = resList[4].toInt()
        binding.progressBarK.progress = resList[5].toInt()

        Log.i("Reception Multicast", "Retour Multi : $resList")
        (activity as? AppCompatActivity)?.supportActionBar?.title = "Résultats"

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ResultatViewModel::class.java)
    }
}
