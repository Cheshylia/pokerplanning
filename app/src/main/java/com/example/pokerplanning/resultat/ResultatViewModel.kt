package com.example.pokerplanning.resultat

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.pokerplanning.multicast.MultiCastAgent

class ResultatViewModel(context: Context) : ViewModel() {
    private val _msgSet = mutableSetOf<String>()
    private var _allmessages : List<String> = listOf("0","0","0","0","0","0")
    private val _multicast = MultiCastAgent(context, this::onReceiveMessage)

    init {
        MultiCastAgent.wifiLock(context)
        _multicast.startReceiveLoop()
    }

    override fun onCleared() {
        _multicast.stopReceiveLoop()
        MultiCastAgent.releaseWifiLock()
        super.onCleared()
    }

    fun onReceiveMessage(msg: String) {
        _msgSet.add(msg)
        Log.i("RECEIVED MESSAGE", "Received message ===================== : $msg")
        _allmessages = _msgSet.toList()
        //TODO : gérer le json
    }
    fun getAllMessages(): List<String>? {
        return _allmessages
    }
}
