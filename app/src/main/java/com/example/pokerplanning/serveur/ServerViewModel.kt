package com.example.pokerplanning.serveur

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.pokerplanning.multicast.MultiCastAgent
//import com.example.pokerplanning.serveur.clientsocketserver.ServerSocket


class ServerViewModel(context: Context): ViewModel() {

    private val _msgSet = mutableSetOf<String>()
    private val _server = ServerSocket(context, this::onReceiveMessage)
    private val _allMessages = MutableLiveData<List<String>>()
    private val _multicastSocket = MultiCastAgent(context, this::onReceiveMessage)
    private val valList = arrayOf("1","2","3","5","8","13")
    private var value1 : Int = 0
    private var value2 : Int = 0
    private var value3 : Int = 0
    private var value5 : Int = 0
    private var value8 : Int = 0
    private var value13 : Int = 0
    private val total : Int = 0


    val messages: LiveData<List<String>> get() = _allMessages

    init {
        MultiCastAgent.wifiLock(context)
//        _multicastSocket.startReceiveLoop()
        _server.startListening()

    }

    override fun onCleared() {
        _server.stopListening()
        super.onCleared()
    }

    fun createList() {
        val myList = listOf(value1, value2, value3, value5, value8, value13)
        //_server.updateList(myList)
        _multicastSocket.send(myList.toString())
    }
    fun stopListening(){
        _server.stopListening()
    }

    fun onReceiveMessage(msg: String) {
        _msgSet.add(msg)
        checkIfInt(msg)
        _allMessages.postValue(_msgSet.toList())
    }

    fun checkIfInt(msg:String){
        if (valList.contains(msg)){
            val msInt:Int = msg.toInt()
            when(msInt){
                1 -> value1++
                2 -> value2++
                3 -> value3++
                5 -> value5++
                8 -> value8++
                13-> value13++
            }
        }

    }


}