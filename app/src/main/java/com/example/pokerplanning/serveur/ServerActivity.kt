package com.example.pokerplanning.serveur

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.example.pokerplanning.NetworkUtils
import com.example.pokerplanning.R
import com.example.pokerplanning.databinding.ServeurActivityBinding
import com.example.pokerplanning.messages.MessageAdapter

class ServerActivity : AppCompatActivity() {
    lateinit var binding: ServeurActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val model = ViewModelProviders.of(this, ServerViewModelFactory(this)).get(ServerViewModel::class.java)

        binding = DataBindingUtil.setContentView(this, R.layout.serveur_activity)

        binding.serverTitle.text = "Create a voting room"

            binding.btnAdminCreate.setOnClickListener {
            binding.serverTitle.text = "server listening on " + NetworkUtils.getIpAddress(this) + ": ${ServerSocket.PORT}"
        }

        binding.btnAdminClear.setOnClickListener {
                view -> model.stopListening()
                binding.serverTitle.text = "server "+ NetworkUtils.getIpAddress(this) + " is closed."
        }

        binding.sendScoresBtn.setOnClickListener {
            model.createList()

        }

        val adapter = MessageAdapter {}
        binding.messageList.adapter = adapter

        model.messages.observe(this, Observer { list ->
            adapter.list = list
        })
    }
}
