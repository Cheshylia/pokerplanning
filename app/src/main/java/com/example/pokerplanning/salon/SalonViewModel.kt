package com.example.pokerplanning.salon

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.pokerplanning.messages.Message

class SalonViewModel(context: Context?): ViewModel() {

    /** Methods for updating the UI **/
    private val _client = context?.let {
        ClientSocket(
            it,
            this::onReceiveMessage,
            this::onConnectionStatus
        )
    }

    private val _msgSet = mutableSetOf<String>()
    private val _allMessages = MutableLiveData<List<String>>()

    private val _connected = MutableLiveData<Boolean>(false)
    val connected : LiveData<Boolean> get() = _connected

    val messages: LiveData<List<String>> get() = _allMessages

    override fun onCleared() {
        _client?.stop("cleared")
        _connected.postValue(false)
        super.onCleared()
    }

    fun onReceiveMessage(msg: String) {
        _msgSet.add(msg)
        _allMessages.postValue(_msgSet.toList())
    }

    fun onConnectionStatus(bool:Boolean) {
        _connected.postValue(bool)
    }

    fun connect(serverIp:String, serverPort:Int?, pseudo:String){
        _client?.connect(serverIp, serverPort, pseudo)
    }

    fun send(msg:String){
        _client?.send(msg)
    }

    fun getClientSocket(): ClientSocket? {
        return _client
    }
//    fun send(vts:Int){
//        _client?.send(vts.toString())
//    }
}
