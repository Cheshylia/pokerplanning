package com.example.pokerplanning.salon

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.fragment.app.Fragment
import com.example.pokerplanning.R
import com.example.pokerplanning.databinding.SalonFragmentBinding
import com.example.pokerplanning.serveur.ServerSocket

class Salon : Fragment() {

    private lateinit var viewModel: SalonViewModel
    private lateinit var binding: SalonFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.inflate(inflater, R.layout.salon_fragment, container, false)

        viewModel = ViewModelProviders.of(this, SalonViewModelFactory(this.context)).get(SalonViewModel::class.java)

        binding.btnConnect.setOnClickListener {

                view -> viewModel.connect(binding.serveurIP.text.toString(), ServerSocket.PORT, binding.pseudo.text.toString())
//                val socket : ClientSocket = model.getClientSocket()!!
                val ip = binding.serveurIP.text.toString()
                val mybundle = bundleOf("ip" to ip)
                view?.findNavController()?.navigate(R.id.action_salon_to_vote, mybundle)


        }

        //getActivity()?.setTitle("Choix du salon")
        (activity as? AppCompatActivity)?.supportActionBar?.title = "Choix du salon"
        
        return binding.root
    }
}
