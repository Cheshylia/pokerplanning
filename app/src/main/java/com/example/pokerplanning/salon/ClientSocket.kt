package com.example.pokerplanning.salon

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.lifecycle.MutableLiveData
import java.io.IOException
import java.net.InetAddress
import java.net.Socket
import java.nio.charset.StandardCharsets
import java.util.concurrent.Executor
import java.util.concurrent.Executors


class ClientSocket(val context: Context, val messageListener: (String) -> Unit, val connectionListener: (Boolean) -> Unit) {
    private val tag = this.javaClass.simpleName

    private lateinit var socket: Socket
    private var mutableSocket = MutableLiveData<Socket> (null)
    /*
    * private lateinit var ip: String
    * private lateinit var port: Int
    * */

    private val mainThread = object : Executor {
        val handler = Handler(Looper.getMainLooper())
        override fun execute(command: Runnable) {
            handler.post(command)
        }
    }

    fun stop(msg:String) {
        socket.close()
    }

    fun getMSoc(): MutableLiveData<Socket> {
        return mutableSocket
    }

    fun connect(serverIp:String, serverPort:Int?, pseudo:String) {
        Executors.newSingleThreadExecutor().execute {
            try {
                val address = InetAddress.getByName(serverIp)
                val port = serverPort ?: 6776
                Log.d(tag, "try connecting to $address:$port")
                socket = Socket(address, port)
                mutableSocket.postValue(socket)
                this.send(pseudo)
                mainThread.execute { connectionListener(true) }
            }
            catch(ex:Exception) {
                val msg = ex.message ?: "unable to connect server"
                mainThread.execute { messageListener(msg) }
                mainThread.execute { connectionListener(false) }
            }
        }
    }

    fun connect(serverIp:String, serverPort:Int?, value : Int) {
        Executors.newSingleThreadExecutor().execute {
            try {
                val address = InetAddress.getByName(serverIp)
                val port = serverPort ?: 6776
                Log.d(tag, "try connecting to $address:$port")
                socket = Socket(address, port)
                mutableSocket.postValue(socket)
                this.send(value.toString())
                mainThread.execute { connectionListener(true) }
            }
            catch(ex:Exception) {
                val msg = ex.message ?: "unable to connect server"
                mainThread.execute { messageListener(msg) }
                mainThread.execute { connectionListener(false) }
            }
        }
    }

    fun getSocket(): Socket {
        return socket
    }

    fun getTag(): String {
        return tag
    }


    fun sendAndReceive(msg: String) {
        Executors.newSingleThreadExecutor().execute {
            try {
                val data = msg.toByteArray(StandardCharsets.UTF_8)
                socket.getOutputStream().write(data)
                socket.getOutputStream().flush()

                val buffer = ByteArray(2048)
                val len = socket.getInputStream().read(buffer)
                val str = String(buffer, 0, len, StandardCharsets.UTF_8)

                //affiche le message reçu dans l'ui
                mainThread.execute { messageListener(str) }

            } catch (e: IOException) {
                val msg = e.message ?: "unable to send to or receive from server"
                mainThread.execute { messageListener(msg) }
                mainThread.execute { connectionListener(false) }
                stop(msg)
            }
        }
    }

    fun send(msg: String) {
        Executors.newSingleThreadExecutor().execute {
            try {
                val data = msg.toByteArray(StandardCharsets.UTF_8)
                socket.getOutputStream().write(data)
                socket.getOutputStream().flush()

            } catch (e: IOException) {
                val msg = e.message ?: "unable to send to or receive from server"
                mainThread.execute { messageListener(msg) }
                mainThread.execute { connectionListener(false) }
                stop(msg)
            }
        }
    }
}
