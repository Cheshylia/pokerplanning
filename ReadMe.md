# ANDROID POKER PLANNING
##### Emma PARDO - Nicolas HERAND

---
##### Lancement : 
 - Connecter tous les mobiles sur le même réseau wifi
 - Ouvrir l'application serveur sur l'un des terminaux et créer un salon
 - Ouvrir l'application client sur les mobiles et se connecter à l'adresse donnée par le serveur

N.B. : le multicast de retour des votes fonctionne mais n'est pas exploité dans
l'application. Lancer l'application depuis Android Studio (par défaut le Client 
est lancé) pour trouver les valeurs retournées dans les logs au moment où le 
serveur clique sur "Envoyer scores"

---
##### APK : 
 L'apk se trouve à la racine du projet sous le nom "app-debug.apk"
 
